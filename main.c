/*
    tabby - Terminal tab system for UNIX-like systems.
    Copyright (C) 2020  Gustaf Alhäll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define _XOPEN_SOURCE 600
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include "main.h"
#include "term.h"

#define READ_MAX 128
#define INVALID_CHAR '?'

struct winsize winsz;
static struct cell **winbuf;

static size_t active;
static size_t ntabs;
static struct tab **tabs;

static int dirty;
static char *shell;
static struct termios defterm;
struct termios curterm;

char const *smcup;
char const *rmcup;

char const *ename;

static void cleanup(void) {
    if (tcsetattr(STDOUT_FILENO, TCSANOW, &defterm) == -1)
        perror(ename);

    if (!closeterm())
        perror(ename);
}

static void exiterror(void) {
    int err = errno;
    cleanup();
    errno = err;
    perror(ename);
    abort();
}

static ssize_t newtab(void) {
    struct tab *tab;
    struct tab **tl;
    int i;
    int chldfd;
    tl = realloc(tabs, sizeof(struct tab *) * (ntabs + 1));
    if (tl == NULL)
        return -1;
    tabs = tl;

    tab = malloc(sizeof(struct tab));
    if (tab == NULL)
        return -1;

    tab->buf = malloc(sizeof(struct cell *));
    if (tab->buf == NULL)
        goto errtab;

    tab->buf[0] = malloc(sizeof(struct cell) * winsz.ws_col);
    if (tab->buf[0] == NULL) {
        free(tab->buf);
        goto errtab;
    }

    for (i = 0; i < winsz.ws_col; i++) {
        tab->buf[0][i].c = ' ';
        tab->buf[0][i].f = DEFAULT_MODE;
    }
    tab->bufsize = 1;
    tab->cx = 0;
    tab->cy = 0;
    tab->scroll = 0;
    tab->mode = DEFAULT_MODE;
    tab->delete = 0;

    tab->tabstop = malloc(sizeof(char) * winsz.ws_col);
    if (tab->tabstop == NULL)
        goto errbuf;

    for (i = 0; i < winsz.ws_col; i++)
        tab->tabstop[i] = (i + 1) % 8 == 0;

    // posix_openpt is standardized in POSIX, unlike forkpty, which is an
    // extension in FreeBSD.
    tab->fd = posix_openpt(O_RDWR);
    if (tab->fd == -1)
        goto errstop;

    if (grantpt(tab->fd) == -1 || unlockpt(tab->fd) == -1)
        goto errpts;

    if (ioctl(tab->fd, TIOCSWINSZ, &winsz) == -1)
        goto errpts;

    chldfd = open(ptsname(tab->fd), O_RDWR);
    if (chldfd == -1)
        goto errpts;

    if (tcsetattr(chldfd, TCSANOW, &defterm) == -1)
        goto errpts;

    tab->pid = fork();
    if (tab->pid == -1) {
        goto errpts;
    } else if (tab->pid == 0) {
        close(tab->fd);

        dup2(chldfd, STDIN_FILENO);
        dup2(chldfd, STDOUT_FILENO);
        dup2(chldfd, STDERR_FILENO);
        setenv("TERM", "tabby", 1);
        execlp(shell, shell, (char *)NULL);
        perror(shell);
        _exit(EXIT_FAILURE);
    }

    close(chldfd);

    tabs[ntabs] = tab;
    return ntabs++;

errpts:
    close(tab->fd);
errstop:
    free(tab->tabstop);
errbuf:
    free(tab->buf[0]);
    free(tab->buf);
errtab:
    free(tab);
    return -1;
}

static void closetab(size_t n) {
    size_t i;
    assert(n < ntabs);

    if (active > 0 && active >= n)
        active--;

    close(tabs[n]->fd);
    for (i = 0; i < tabs[n]->bufsize; i++) {
        free(tabs[n]->buf[i]);
    }
    free(tabs[n]->tabstop);
    free(tabs[n]->buf);
    free(tabs[n]);
    for (n++; n < ntabs; n++) {
        tabs[n-1] = tabs[n];
    }

    ntabs--;
}

static int writehead(void) {
    size_t i;
    int n;
    size_t p = 0;
    char buf[16];
    n = writeseq(buf, sizeof(buf), cup, 2, 0, 0);
    if (write(STDOUT_FILENO, buf, n) == -1)
        return 0;

    if (setaf != NULL && setab != NULL) {
        n = writeseq(buf, sizeof(buf), setaf, 1, 0);
        n += writeseq(&buf[n], sizeof(buf) - n, setab, 1, 7);
        if (write(STDOUT_FILENO, buf, n) == -1)
            return 0;
    }

    for (i = 0; i < ntabs; i++) {
        if (i == active) {
            if (bold != NULL)
                if (write(STDOUT_FILENO, bold, strlen(bold)) == -1)
                    return 0;

            if (setaf != NULL && setab != NULL) {
                n = writeseq(buf, sizeof(buf), setaf, 1, 7);
                n += writeseq(&buf[n], sizeof(buf) - n, setab, 1, 0);
                if (write(STDOUT_FILENO, buf, n) == -1)
                    return 0;
            }
        }

        n = snprintf(buf, sizeof(buf), " %d ", i + 1);
        if (write(STDOUT_FILENO, buf, n) == -1)
            return 0;
        p += n;

        if (i == active) {
            if (sgr0 != NULL)
                if (write(STDOUT_FILENO, sgr0, strlen(sgr0)) == -1)
                    return 0;

            if (setaf != NULL && setab != NULL) {
                n = writeseq(buf, sizeof(buf), setaf, 1, 0);
                n += writeseq(&buf[n], sizeof(buf) - n, setab, 1, 7);
                if (write(STDOUT_FILENO, buf, n) == -1)
                    return 0;
            }
        }
    }

    while (p++ < winsz.ws_col)
        if (write(STDOUT_FILENO, " ", 1) == -1)
            return 0;

    return (sgr0 != NULL) ? (write(STDOUT_FILENO, sgr0, strlen(sgr0)) != -1) : 1;
}

static void handlesig(int sig) {
    size_t i, j, k;
    int status;
    pid_t pid;
    struct winsize newsz;

    // Dirty: mark the screen for a full refresh
    dirty = 1;

    if (sig == SIGCHLD) {
retrywait:
        pid = wait(&status);
        if (pid == -1) {
            if (errno == EINTR)
                goto retrywait;
            exiterror();
        }
        for (i = 0; i < ntabs; i++) {
            if (tabs[i]->pid == pid) {
                // Don't free here; instead, mark for deletion and free when appropriate.
                if (WIFEXITED(status) || WIFSIGNALED(status))
                    tabs[i]->delete = 1;
            }
        }
    } else {
retrygwinsz:
        if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &newsz) == -1) {
            if (errno == EINTR)
                goto retrygwinsz;
            exiterror();
        }
        newsz.ws_row--;

        for (i = 0; i < ntabs; i++) {
            for (j = 0; j < tabs[i]->bufsize; j++) {
retryrealloc:
                tabs[i]->buf[j] = realloc(tabs[i]->buf[j], sizeof(struct cell) * newsz.ws_col);
                if (tabs[i]->buf[j] == NULL) {
                    if (errno == EINTR)
                        goto retryrealloc;
                    exiterror();
                }

                for (k = winsz.ws_col; k < newsz.ws_col; k++) {
                    tabs[i]->buf[j][k].c = ' ';
                    tabs[i]->buf[j][k].f = DEFAULT_MODE;
                }
            }

retrytabstop:
            tabs[i]->tabstop = realloc(tabs[i]->tabstop, sizeof(char) * newsz.ws_col);
            if (tabs[i]->tabstop == NULL) {
                if (errno == EINTR)
                    goto retrytabstop;
                exiterror();
            }
            for (j = winsz.ws_col; j < newsz.ws_col; j++)
                tabs[i]->tabstop[i] = (i + 1) % 8 == 0;

retryswinsz:
            if (ioctl(tabs[i]->fd, TIOCSWINSZ, &newsz) == -1) {
                if (errno == EINTR)
                    goto retryswinsz;
                exiterror();
            }
        }

        winsz.ws_row = newsz.ws_row;
        winsz.ws_col = newsz.ws_col;
    }
}

static int refresh(void) {
    int i, j;
    int l = -1;
    uint32_t c;
    uint16_t f;
    uint16_t mode = DEFAULT_MODE;
    struct cell **tbuf = &tabs[active]->buf[tabs[active]->scroll];
    size_t bufsz;
    char *buf;

    bufsz = winsz.ws_row * winsz.ws_col;
    buf = malloc(sizeof(char) * bufsz);
    if (buf == NULL)
        return 0;

    l = writeseq(buf, bufsz, sgr0, 0);
    if (write(STDOUT_FILENO, buf, l) == -1) {
        free(buf);
        return 0;
    }
    for (i = 0; i < winsz.ws_row; i++) {
        for (j = 0; j < winsz.ws_col; j++) {
            c = ' ';
            f = DEFAULT_MODE;
            if ((size_t)i < tabs[active]->bufsize - tabs[active]->scroll) {
                c = tbuf[i][j].c;
                f = tbuf[i][j].f;
            }
            if (l == -1) {
                if (winbuf[i][j].c != c || winbuf[i][j].f != f) {
                    winbuf[i][j].c = c;
                    winbuf[i][j].f = f;
                    l = writeseq(buf, bufsz, cup, 2, i+1, j);
                    if (f != mode) {
                        if (sgr != NULL && (mode & ~(FG_COLOR | BG_COLOR)) != (f & ~(FG_COLOR | BG_COLOR)))
                            l += writeseq(&buf[l], bufsz-l, sgr, 9, 0, f & UNDERLINE, f & NEGATIVE, f & BLINK, f & FAINT, f & BOLD, f & HIDDEN, 0, 0);
                        if (setaf != NULL && (mode & FG_COLOR) != (f & FG_COLOR))
                            l += writeseq(&buf[l], bufsz-l, setaf, 1, f >> FG_SHIFT);
                        if (setab != NULL && (mode & BG_COLOR) != (f & BG_COLOR))
                            l += writeseq(&buf[l], bufsz-l, setab, 1, f >> BG_SHIFT);
                        mode = f;
                    }
                    buf[l++] = c;
                }
            } else {
                if (winbuf[i][j].c == c && winbuf[i][j].f == f) {
                    if (write(STDOUT_FILENO, buf, l) == -1) {
                        free(buf);
                        return 0;
                    }
                    l = -1;
                } else {
                    winbuf[i][j].c = c;
                    winbuf[i][j].f = f;
                    if (f != mode) {
                        if (sgr != NULL && (mode & ~(FG_COLOR | BG_COLOR)) != (f & ~(FG_COLOR | BG_COLOR)))
                            l += writeseq(&buf[l], bufsz-l, sgr, 9, 0, f & UNDERLINE, f & NEGATIVE, f & BLINK, f & FAINT, f & BOLD, f & HIDDEN, 0, 0);
                        if (setaf != NULL && (mode & FG_COLOR) != (f & FG_COLOR))
                            l += writeseq(&buf[l], bufsz-l, setaf, 1, (f & FG_COLOR) >> FG_SHIFT);
                        if (setab != NULL && (mode & BG_COLOR) != (f & BG_COLOR))
                            l += writeseq(&buf[l], bufsz-l, setab, 1, (f & BG_COLOR) >> BG_SHIFT);
                        mode = f;
                    }
                    buf[l++] = c;
                }
            }
        }

        if (l != -1) {
            if (write(STDOUT_FILENO, buf, l) == -1) {
                free(buf);
                return 0;
            }
            l = -1;
        }
    }

    l = writeseq(buf, bufsz, cup, 2, (int)tabs[active]->cy + 1, (int)tabs[active]->cx);
    if (write(STDOUT_FILENO, buf, l) == -1) {
        free(buf);
        return 0;
    }
    free(buf);
    return 1;
}

int main(int argc, char **argv) {
    fd_set rs;
    ssize_t i, j, k;
    ssize_t n, m;
    int c;
    char t = 0;
    struct sigaction sig;
    char buf[READ_MAX];
    struct tab *tab;
    memset(&sig, 0, sizeof(sig));
    sig.sa_handler = handlesig;
    sig.sa_flags = 0;

    ename = argv[0];

    if (sigaction(SIGCHLD, &sig, NULL) == -1) {
        perror(ename);
        return EXIT_FAILURE;
    }
    if (sigaction(SIGWINCH, &sig, NULL) == -1) {
        perror(ename);
        return EXIT_FAILURE;
    }

    shell = getenv("SHELL");
    if (shell == NULL) {
        fprintf(stderr, "%s: SHELL is not set\n", ename);
        return EXIT_FAILURE;
    }

    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &winsz) == -1) {
        perror(ename);
        return EXIT_FAILURE;
    }
    winsz.ws_row--;

    if (tcgetattr(STDOUT_FILENO, &defterm) == -1) {
        perror(ename);
        return EXIT_FAILURE;
    }

    if (!initterm()) {
        fprintf(stderr, "%s: failed to initialize terminal\n", ename);
        return EXIT_FAILURE;
    }

    if (atexit(cleanup) != 0) {
        fprintf(stderr, "%s: failed to set cleanup function\n", ename);
        return EXIT_FAILURE;
    }

    memcpy(&curterm, &defterm, sizeof(struct termios));
    curterm.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    curterm.c_oflag &= ~OPOST;
    curterm.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    curterm.c_cflag &= ~(CSIZE | PARENB);
    curterm.c_cflag |= CS8;
    if (tcsetattr(STDOUT_FILENO, TCSANOW, &curterm) == -1) {
        perror(ename);
        return EXIT_FAILURE;
    }

    winbuf = malloc(sizeof(struct cell *) * winsz.ws_row);
    if (winbuf == NULL) {
        tcsetattr(STDOUT_FILENO, TCSANOW, &defterm);
        fprintf(stderr, "%s: out of memory", ename);
        return EXIT_FAILURE;
    }

    for (i = 0; i < winsz.ws_row; i++) {
        winbuf[i] = malloc(sizeof(struct cell) * winsz.ws_col);
        if (winbuf[i] == NULL) {
            tcsetattr(STDOUT_FILENO, TCSANOW, &defterm);
            fprintf(stderr, "%s: out of memory", ename);
            return EXIT_FAILURE;
        }

        for (j = 0; j < winsz.ws_col; j++) {
            winbuf[i][j].c = ' ';
            winbuf[i][j].f = DEFAULT_MODE;
        }
    }

    n = newtab();
    if (n == -1) {
        tcsetattr(STDOUT_FILENO, TCSANOW, &defterm);
        perror(ename);
        return EXIT_FAILURE;
    }
    active = n;

    if (!writehead())
        exiterror();

    while (ntabs > 0) {
        if (dirty) {
            // This will happen when either the screen has been resized or a
            // child has exited. Either way, the header needs to be updated.
            for (i = 0; (size_t)i < ntabs; i++) {
                if (tabs[i]->delete) {
                    closetab(i--);
                    continue;
                }
            }

            if (ntabs <= 0)
                break;

            if (!writehead() || !refresh()) {
                if (errno == EINTR)
                    continue;
                exiterror();
            }
            dirty = 0;
        }

        FD_SET(STDIN_FILENO, &rs);
        m = STDIN_FILENO;
        for (i = 0; (size_t)i < ntabs; i++) {
            if (m < tabs[i]->fd)
                m = tabs[i]->fd;
            FD_SET(tabs[i]->fd, &rs);
        }

        if (m == STDIN_FILENO)
            break;

        if (select(m + 1, &rs, NULL, NULL, NULL) == -1) {
            if (errno == EINTR)
                continue;
            exiterror();
        }

        if (FD_ISSET(STDIN_FILENO, &rs)) {
            n = read(STDIN_FILENO, &buf, READ_MAX);
            if (n == -1) {
                if (errno == EINTR)
                    continue;
                exiterror();
            }

            for (i = 0; i < n; i++) {
                if (buf[i] == 0x14) {
                    t = 1;
                } else if (t) {
                    switch (buf[i]) {
                        case 'n':
                            if (++active >= ntabs)
                                active = 0;

                            if (!writehead() || !refresh()) {
                                if (errno == EINTR)
                                    continue;
                                exiterror();
                            }
                            break;
                        case 'p':
                            if ((ssize_t)--active < 0)
                                active = ntabs - 1;

                            if (!writehead() || !refresh()) {
                                if (errno == EINTR)
                                    continue;
                                exiterror();
                            }
                            break;
                        case 't':
                            m = newtab();
                            if (m == -1) {
                                if (errno == EINTR)
                                    continue;
                                exiterror();
                            }

                            active = m;
                            if (!writehead() || !refresh()) {
                                if (errno == EINTR)
                                    continue;
                                exiterror();
                            }
                            break;
                        case 'c':
                            closetab(active);
                            if (ntabs > 0)
                                if (!writehead() || !refresh()) {
                                    if (errno == EINTR)
                                        continue;
                                    exiterror();
                                }
                            break;
                        case 0x14:
                            if (write(tabs[active]->fd, &buf[i], 1) == -1) {
                                if (errno == EINTR || errno == EIO)
                                    continue;
                                exiterror();
                            }
                            break;
                    }
                    t = 0;
                } else if (buf[i] == '\x1b') {
                    for (j = 0; (size_t)j < sizeof(inmap) / sizeof(inmap[0]); j++) {
                        if (inmap[j][0] != NULL && (size_t)(n-i) >= strlen(inmap[j][0]) && strncmp(&buf[i], inmap[j][0], strlen(inmap[j][0])) == 0) {
                            if (write(tabs[active]->fd, inmap[j][1], strlen(inmap[j][1])) == -1) {
                                if (errno == EINTR || errno == EIO)
                                    continue;
                                exiterror();
                            }
                            i += strlen(inmap[j][1]) - 1;
                            break;
                        }
                    }

                    if (j == sizeof(inmap) / sizeof(inmap[0])) {
                        if (write(tabs[active]->fd, &buf[i], 1) == -1) {
                            if (errno == EINTR || errno == EIO)
                                continue;
                            exiterror();
                        }
                    }
                } else {
                    if (write(tabs[active]->fd, &buf[i], 1) == -1) {
                        if (errno == EINTR || errno == EIO)
                            continue;
                        exiterror();
                    }
                }
            }
        } else {
            for (i = 0; i < (ssize_t)ntabs; i++) {
                if (FD_ISSET(tabs[i]->fd, &rs)) {
                    tab = tabs[i];
                    break;
                }
            }
            n = read(tab->fd, &buf, READ_MAX);
            if (n == -1) {
                if (errno == EINTR || errno == EIO)
                    continue;
                exiterror();
            }

            for (i = 0; i < n; i += m) {
                // TODO: Handle multiple encodings.
                if (buf[i] & 0x80) {
                    if ((buf[i] & 0xe0) == 0xc0) {
                        if (i + 1 >= n)
                            c = INVALID_CHAR;
                        else
                            c = ((buf[i] & 0x1f) << 6) |
                                (buf[i+1] & 0x3f);
                        m = 2;
                    } else if ((buf[i] & 0xf0) == 0xe0) {
                        if (i + 2 >= n)
                            c = INVALID_CHAR;
                        else
                            c = ((buf[i] & 0x0f) << 12) |
                                ((buf[i+1] & 0x3f) << 6) |
                                (buf[i+2] & 0x3f);
                        m = 3;
                    } else if ((buf[i] & 0xf8) == 0xf0) {
                        if (i + 3 >= n)
                            c = INVALID_CHAR;
                        else
                            c = ((buf[i] & 0x07) << 18) |
                                ((buf[i+1] & 0x3f) << 12) |
                                ((buf[i+2] & 0x3f) << 6) |
                                (buf[i+3] & 0x3f);
                        m = 4;
                    }
                } else {
                    c = buf[i];
                    m = 1;
                }

                switch (c) {
                    case '\n':
                        tab->cy++;
                        break;

                    case '\r':
                        tab->cx = 0;
                        break;

                    case '\b':
                        tab->cx--;
                        if (tab->cx < 0) {
                            tab->cx = winsz.ws_col - 1;
                            tab->cy--;
                        }
                        break;

                    case '\t':
                        while (tab->cx < winsz.ws_col)
                            if (tab->tabstop[++tab->cx])
                                break;
                        break;

                    case '\f':
                        tab->cx = 0;
                        tab->cy = 0;
                        tab->scroll = tab->bufsize;
                        break;

                    case '\x1b':
                        m = parseesc(&buf[i], n - i, tab);
                        if (m == 0) {
                            if (errno == EINTR)
                                continue;
                            exiterror();
                        }
                        break;

                    default:
                        if (c >= 0x20) {
                            tab->buf[tab->cy + tab->scroll][tab->cx].f = tab->mode;
                            tab->buf[tab->cy + tab->scroll][tab->cx++].c = c;
                            if (tab->cx >= winsz.ws_col) {
                                tab->cx = 0;
                                tab->cy++;
                            }
                        }
                        break;
                }
                if (tab->cy >= winsz.ws_row) {
                    tab->scroll++;
                    tab->cy--;
                } else if (tab->cy < 0) {
                    if (tab->scroll > 0)
                        tab->scroll--;
                    tab->cy++;
                }

                if (tab->cy + tab->scroll >= tab->bufsize) {
                    tab->buf = realloc(tab->buf, sizeof(struct cell *) * (tab->cy + tab->scroll + 1));
                    if (tab->buf == NULL) {
                        if (errno == EINTR)
                            continue;
                        exiterror();
                    }

                    for (j = tab->bufsize; (size_t)j <= tab->cy + tab->scroll; j++) {
                        tab->buf[j] = malloc(sizeof(struct cell) * winsz.ws_col);
                        if (tab->buf[j] == NULL) {
                            if (errno == EINTR)
                                continue;
                            exiterror();
                        }

                        for (k = 0; k < winsz.ws_col; k++) {
                            tab->buf[j][k].c = ' ';
                            tab->buf[j][k].f = DEFAULT_MODE;
                        }
                    }
                    tab->bufsize = tab->cy + tab->scroll + 1;
                }
            }
            if (!refresh()) {
                if (errno == EINTR)
                    continue;
                exiterror();
            }
        }
    }

    free(tabs);
    return EXIT_SUCCESS;
}
