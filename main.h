/*
    tabby - Terminal tab system for UNIX-like systems.
    Copyright (C) 2020  Gustaf Alhäll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAIN_H
#define MAIN_H

#include <pty.h>
#include <stdint.h>
#include <sys/types.h>

#define DEFAULT_MODE 0x0700
#define FG_COLOR 0x0f00
#define FG_SHIFT 8
#define BG_COLOR 0xf000
#define BG_SHIFT 12
#define BOLD 0x0001
#define FAINT 0x0002
#define UNDERLINE 0x0004
#define BLINK 0x0008
#define NEGATIVE 0x0010
#define HIDDEN 0x0020

struct cell {
    uint16_t f;
    uint32_t c;
};

struct tab {
    int delete;
    int fd;
    pid_t pid;

    size_t bufsize;
    struct cell **buf;
    int16_t cx, cy;
    size_t scroll;
    uint16_t mode;

    char *tabstop;
};

extern struct winsize winsz;

size_t parseesc(char const *buf, size_t len, struct tab *tab);

#endif
