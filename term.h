/*
    tabby - Terminal tab system for UNIX-like systems.
    Copyright (C) 2020  Gustaf Alhäll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TERM_H
#define TERM_H

extern char const *ename;

extern char const *bold;
extern char const *clear;
extern char const *cup;
extern char const *rmcup;
extern char const *setab;
extern char const *setaf;
extern char const *setb;
extern char const *setf;
extern char const *sgr;
extern char const *sgr0;
extern char const *smcup;

char const *inmap[28][2];

int initterm(void);
int closeterm(void);
int writeseq(char *buf, size_t bufsz, char const *seq, size_t nargs, ...);

#endif

