/*
    tabby - Terminal tab system for UNIX-like systems.
    Copyright (C) 2020  Gustaf Alhäll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <term.h>
#include <unistd.h>
#include "term.h"

char const *bold;
char const *clear;
char const *cup;
char const *rmcup;
char const *setab;
char const *setaf;
char const *setb;
char const *setf;
char const *sgr;
char const *sgr0;
char const *smcup;

char const *inmap[28][2] = {
    { NULL, "\x1b[1~" }, // f1
    { NULL, "\x1b[2~" }, // f2
    { NULL, "\x1b[3~" }, // f3
    { NULL, "\x1b[4~" }, // f4
    { NULL, "\x1b[5~" }, // f5
    { NULL, "\x1b[6~" }, // f6
    { NULL, "\x1b[7~" }, // f7
    { NULL, "\x1b[8~" }, // f8
    { NULL, "\x1b[9~" }, // f9
    { NULL, "\x1b[10~" }, // f10
    { NULL, "\x1b[11~" }, // f11
    { NULL, "\x1b[12~" }, // f12

    { NULL, "\x1b[@" }, // ich1
    { NULL, "\x1b[L" }, // IC
    { NULL, "\x1b[P" }, // dch1
    { NULL, "\x1b[M" }, // DC
    { NULL, "\x1b[G" }, // khome
    { NULL, "\x1b[H" }, // kHOM
    { NULL, "\x1b[g" }, // kend
    { NULL, "\x1b[h" }, // kEND
    { NULL, "\x1b[S" }, // kpp
    { NULL, "\x1b[2S" }, // kPRV
    { NULL, "\x1b[T" }, // knp
    { NULL, "\x1b[2T" }, // kNXT

    { NULL, "\x1b[A" }, // kcuu1
    { NULL, "\x1b[B" }, // kcud1
    { NULL, "\x1b[C" }, // kcuf1
    { NULL, "\x1b[D" }, // kcub1
};

int initterm(void) {
    int status;
    char kfs[5];
    int i;
    if (setupterm(NULL, STDOUT_FILENO, &status) == -1)
        return 0;

    setaf = tigetstr("setaf");
    setab = tigetstr("setab");
    setf = tigetstr("setf");
    setb = tigetstr("setb");
    sgr = tigetstr("sgr");
    sgr0 = tigetstr("sgr0");

    cup = tigetstr("cup");
    if (cup == NULL)
        return 0;

    clear = tigetstr("clear");
    if (clear == NULL)
        return 0;

    for (i = 0; i < 12; i++) {
        snprintf(kfs, sizeof(kfs) / sizeof(kfs[0]), "kf%d", i+1);
        inmap[i][0] = tigetstr(kfs);
    }

    inmap[12][0] = tigetstr("kich1");
    inmap[13][0] = tigetstr("kIC");
    inmap[14][0] = tigetstr("kdch1");
    inmap[15][0] = tigetstr("kDC");
    inmap[16][0] = tigetstr("khome");
    inmap[17][0] = tigetstr("kHOM");
    inmap[18][0] = tigetstr("kend");
    inmap[19][0] = tigetstr("kEND");
    inmap[20][0] = tigetstr("kpp");
    inmap[21][0] = tigetstr("kPRV");
    inmap[22][0] = tigetstr("knp");
    inmap[23][0] = tigetstr("kNXT");

    inmap[24][0] = tigetstr("kcuu1");
    inmap[25][0] = tigetstr("kcud1");
    inmap[26][0] = tigetstr("kcuf1");
    inmap[27][0] = tigetstr("kcub1");

    // Check if there is an alternative buf available. If so, use it.
    smcup = tigetstr("smcup");
    rmcup = tigetstr("rmcup");
    if (smcup != NULL && rmcup != NULL) {
        return write(STDOUT_FILENO, smcup, strlen(smcup)) != -1;
    } else {
        return write(STDOUT_FILENO, clear, strlen(clear)) != -1;
    }
}

int closeterm(void) {
    if (smcup != NULL && rmcup != NULL) {
        return write(STDOUT_FILENO, rmcup, strlen(rmcup)) != -1;
    } else {
        return write(STDOUT_FILENO, clear, strlen(clear)) != -1;
    }
}

int writeseq(char *buf, size_t bufsz, char const *seq, size_t nargs, ...) {
    va_list args;
    size_t len;
    va_start(args, nargs);

    switch (nargs) {
        case 0:
            seq = tiparm(seq);
            break;
        case 1:
            seq = tiparm(seq, va_arg(args, int));
            break;
        case 2:
            seq = tiparm(seq, va_arg(args, int), va_arg(args, int));
            break;
        case 9:
            seq = tiparm(seq, va_arg(args, int), va_arg(args, int), va_arg(args, int),
                    va_arg(args, int), va_arg(args, int), va_arg(args, int),
                    va_arg(args, int), va_arg(args, int), va_arg(args, int));
            break;
    }

    va_end(args);
    len = strlen(seq);
    memcpy(buf, seq, len < bufsz ? len : bufsz);
    return len;
}
