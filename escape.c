/*
    tabby - Terminal tab system for UNIX-like systems.
    Copyright (C) 2020  Gustaf Alhäll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "term.h"
#include "main.h"

struct escape {
    char const *seq;
    char len;
    int (*func)(unsigned short *param, size_t nparam, struct tab *tab);
};

static int cursleft(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->cx -= param[0];
    return 1;
}

static int cursright(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->cx += param[0];
    return 1;
}

static int cursup(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->cy -= param[0];
    return 1;
}

static int cursdown(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->cy += param[0];
    return 1;
}

static int cursnl(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->cy += param[0];
    tab->cx = 0;
    return 1;
}

static int curspl(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->cy -= param[0];
    tab->cx = 0;
    return 1;
}

static int setx(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->cx = param[0] - 1;
    return 1;
}

static int sety(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->cy = param[0] - 1;
    return 1;
}

static int setpos(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    if (nparam < 2)
        param[1] = 1;
    tab->cy = param[0] - 1;
    tab->cx = param[1] - 1;
    return 1;
}

static int fwtab(unsigned short *param, size_t nparam, struct tab *tab) {
    unsigned short i;
    if (nparam < 1)
        param[0] = 1;

    for (i = 0; i < param[0] && tab->cx < winsz.ws_col - 1; tab->cx++)
        if (tab->tabstop[tab->cx])
            i++;
    return 1;
}

static int backtab(unsigned short *param, size_t nparam, struct tab *tab) {
    unsigned short i;
    if (nparam < 1)
        param[0] = 1;

    for (i = 0; i < param[0] && tab->cx > 0; tab->cx--)
        if (tab->tabstop[tab->cx])
            i++;
    return 1;
}

static int tabctrl(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i, j;
    if (nparam < 1) {
        param[0] = 0;
        nparam = 1;
    }

    for (i = 0; i < nparam; i++) {
        switch (param[i]) {
            case 0:
                tab->tabstop[tab->cx] = 1;
                break;
            case 2:
                tab->tabstop[tab->cx] = 0;
                break;
            case 4:
            case 5:
            case 6:
                for (j = 0; j < winsz.ws_col; j++)
                    tab->tabstop[j] = 0;
        }
    }
    return 1;
}

static int cleartab(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i;
    if (nparam < 1)
        param[0] = 0;

    switch (param[0]) {
        case 0:
            tab->tabstop[tab->cx] = 0;
            break;
        case 2:
        case 3:
        case 5:
            for (i = 0; i < winsz.ws_col; i++)
                tab->tabstop[i] = 0;
    }
    return 1;
}

static int delchar(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i;
    if (nparam < 1)
        param[0] = 1;

    for (i = tab->cx; i + param[0] < winsz.ws_col; i++) {
        tab->buf[tab->cy][i].c = tab->buf[tab->cy][i + param[0]].c;
        tab->buf[tab->cy][i].f = tab->buf[tab->cy][i + param[0]].f;
    }

    while (i < winsz.ws_col) {
        tab->buf[tab->cy][i].c = ' ';
        tab->buf[tab->cy][i++].f = 0;
    }
    return 1;
}

static int erasechar(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i;
    if (nparam < 1)
        param[0] = 1;

    param[0] += tab->cx;
    for (i = tab->cx; i < winsz.ws_col && i < param[0]; i++) {
        tab->buf[tab->cy][i].c = ' ';
        tab->buf[tab->cy][i].f = 0;
    }
    return 1;
}

static int inschar(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i;
    if (nparam < 1)
        param[0] = 1;

    for (i = winsz.ws_col - 1; i >= (size_t)tab->cx - param[0]; i--) {
        tab->buf[tab->cy][i + param[0]].c = tab->buf[tab->cy][i].c;
        tab->buf[tab->cy][i + param[0]].f = tab->buf[tab->cy][i].f;
    }

    for (i = 0; i < winsz.ws_col && i < param[0]; i++) {
        tab->buf[tab->cy][i + tab->cx].c = ' ';
        tab->buf[tab->cy][i + tab->cx].f = 0;
    }
    return 1;
}

static int delline(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i;
    if (nparam < 1)
        param[0] = 1;

    for (i = tab->cy; i < tab->bufsize - param[0]; i++)
        tab->buf[i] = tab->buf[i + param[0]];
    while (i < tab->bufsize)
        free(tab->buf[i++]);

    tab->bufsize -= param[0];
    tab->buf = realloc(tab->buf, sizeof(struct cell *) * tab->bufsize);
    if (tab->buf == NULL)
        return 0;
    return 1;
}

static int eraseline(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i;
    if (nparam < 1)
        param[0] = 0;

    switch (param[0]) {
        case 0:
            for (i = tab->cx; i < winsz.ws_col; i++) {
                tab->buf[tab->cy][i].c = ' ';
                tab->buf[tab->cy][i].f = 0;
            }
            break;
        case 1:
            for (i = 0; i <= (size_t)tab->cx; i++) {
                tab->buf[tab->cy][i].c = ' ';
                tab->buf[tab->cy][i].f = 0;
            }
            break;
        case 2:
            for (i = 0; i < winsz.ws_col; i++) {
                tab->buf[tab->cy][i].c = ' ';
                tab->buf[tab->cy][i].f = 0;
            }
            break;
    }
    return 1;
}

static int insline(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i, j;

    if (nparam < 1)
        param[0] = 1;

    tab->buf = realloc(tab->buf, sizeof(struct cell *) * (tab->bufsize + param[0]));
    if (tab->buf == NULL)
        return 0;

    for (i = tab->bufsize - 1; i >= (size_t)tab->cy; i--)
        tab->buf[i + param[0]] = tab->buf[i];
    for (i = 0; i < param[0]; i++) {
        tab->buf[i + tab->cy] = malloc(sizeof(struct cell) * winsz.ws_col);
        if (tab->buf[i + tab->cy] == NULL)
            return 0;

        for (j = 0; j < winsz.ws_col; j++) {
            tab->buf[i + tab->cy][j].c = ' ';
            tab->buf[i + tab->cy][j].f = 0;
        }
    }
    return 1;
}

static int erasepage(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i, j;
    if (nparam < 1)
        param[0] = 0;

    switch (param[0]) {
        case 0:
            for (i = tab->cx; i < winsz.ws_col; i++) {
                tab->buf[tab->cy][i].c = ' ';
                tab->buf[tab->cy][i].f = 0;
            }
            for (i = tab->cy; i < tab->bufsize; i++) {
                for (j = 0; j < winsz.ws_col; j++) {
                    tab->buf[i][j].c = ' ';
                    tab->buf[i][j].f = 0;
                }
            }
            break;

        case 1:
            for (i = tab->scroll; i < (size_t)tab->cy; i++) {
                for (j = 0; j < winsz.ws_col; j++) {
                    tab->buf[i][j].c = ' ';
                    tab->buf[i][j].f = 0;
                }
            }
            for (i = 0; i <= (size_t)tab->cx; i++) {
                tab->buf[tab->cy][i].c = ' ';
                tab->buf[tab->cy][i].f = 0;
            }
            break;

        case 2:
            tab->cy -= tab->scroll;
            tab->scroll = 0;
            for (i = tab->bufsize - 1; i > (size_t)tab->cy; i--)
                free(tab->buf[i]);

            tab->bufsize = i + 1;
            for (i = 0; i < tab->bufsize; i++) {
                for (j = 0; j < winsz.ws_col; j++) {
                    tab->buf[i][j].c = ' ';
                    tab->buf[i][j].f = 0;
                }
            }
            break;
    }
    return 1;
}

static int scrollup(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->scroll -= param[0];
    return 1;
}

static int scrolldown(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->scroll += param[0];
    return 1;
}

static int pageup(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->scroll -= winsz.ws_row * param[0];
    return 1;
}

static int pagedown(unsigned short *param, size_t nparam, struct tab *tab) {
    if (nparam < 1)
        param[0] = 1;
    tab->scroll += winsz.ws_row * param[0];
    return 1;
}

static int setmode(unsigned short *param, size_t nparam, struct tab *tab) {
    size_t i;
    if (nparam < 1) {
        param[0] = 0;
        nparam = 1;
    }

    for (i = 0; i < nparam; i++) {
        if (param[i] >= 30 && param[i] <= 37) {
            tab->mode &= ~FG_COLOR;
            tab->mode |= (param[i] - 30) << FG_SHIFT;
        } else if (param[i] >= 40 && param[i] <= 47) {
            tab->mode &= ~BG_COLOR;
            tab->mode |= (param[i] - 40) << BG_SHIFT;
        } else switch (param[i]) {
            case 0:
                tab->mode = DEFAULT_MODE;
                break;
            case 1:
                tab->mode |= BOLD;
                break;
            case 2:
                tab->mode |= FAINT;
                break;
            case 4:
                tab->mode |= UNDERLINE;
                break;
            case 5:
                tab->mode |= BLINK;
                break;
            case 7:
                tab->mode |= NEGATIVE;
                break;
            case 8:
                tab->mode |= HIDDEN;
                break;
            case 22:
                tab->mode &= ~BOLD;
                break;
            case 25:
                tab->mode &= ~BLINK;
                break;
            case 27:
                tab->mode &= ~NEGATIVE;
                break;
            case 28:
                tab->mode &= ~HIDDEN;
                break;
            case 39:
                tab->mode |= FG_COLOR;
                break;
            case 49:
                tab->mode |= BG_COLOR;
                break;
        }
    }
    return 1;
}

static struct escape seqs[] = {
    { (char const[1]){ '@' }, 1, inschar },
    { (char const[1]){ 'A' }, 1, cursup },
    { (char const[1]){ 'B' }, 1, cursdown },
    { (char const[1]){ 'C' }, 1, cursright },
    { (char const[1]){ 'D' }, 1, cursleft },
    { (char const[1]){ 'E' }, 1, cursnl },
    { (char const[1]){ 'F' }, 1, curspl },
    { (char const[1]){ 'G' }, 1, setx },
    { (char const[1]){ 'H' }, 1, setpos },
    { (char const[1]){ 'I' }, 1, fwtab },
    { (char const[1]){ 'J' }, 1, erasepage },
    { (char const[1]){ 'K' }, 1, eraseline },
    { (char const[1]){ 'L' }, 1, insline },
    { (char const[1]){ 'M' }, 1, delline },
    { (char const[1]){ 'P' }, 1, delchar },
    { (char const[1]){ 'S' }, 1, scrollup },
    { (char const[1]){ 'T' }, 1, scrolldown },
    { (char const[1]){ 'U' }, 1, pagedown },
    { (char const[1]){ 'V' }, 1, pageup },
    { (char const[1]){ 'W' }, 1, tabctrl },
    { (char const[1]){ 'X' }, 1, erasechar },
    { (char const[1]){ 'Z' }, 1, backtab },
    { (char const[1]){ 'g' }, 1, cleartab },
    { (char const[1]){ 'm' }, 1, setmode },
};

size_t parseesc(char const *buf, size_t len, struct tab *tab) {
    size_t i, j;
    size_t nparam;
    unsigned short param[9];

    assert(buf[0] == 0x1b);
    if (buf[1] != '[')
        return 1;

    nparam = 0;
    i = 2;
    while (i < len && (buf[i] >= '0' && buf[i] <= '9')) {
        param[nparam++] = 0;
        while (i < len && (buf[i] >= '0' && buf[i] <= '9'))
            param[nparam-1] = param[nparam-1] * 10 + buf[i++] - '0';

        if (i < len && buf[i] == ';')
            i++;
    }

    if (i == len)
        return 1;

    for (j = 0; j < sizeof(seqs) / sizeof(seqs[0]); j++) {
        if (memcmp(&buf[i], seqs[j].seq, seqs[j].len) == 0) {
            if (!seqs[j].func(param, nparam, tab))
                return 0;
            break;
        }
    }

    if (j >= sizeof(seqs) / sizeof(seqs[0]))
        return 1;

    if ((ssize_t)tab->scroll < 0)
        tab->scroll = 0;
    if (tab->scroll >= tab->bufsize)
        tab->scroll = tab->bufsize - 1;
    if (tab->cx < 0)
        tab->cx = 0;
    if (tab->cx >= winsz.ws_col)
        tab->cx = winsz.ws_col - 1;
    if (tab->cy < (int32_t)tab->scroll)
        tab->cy = tab->scroll;
    if (tab->cy >= (int32_t)tab->scroll + winsz.ws_row)
        tab->cy = tab->scroll + winsz.ws_row - 1;

    return i + 1;
}
